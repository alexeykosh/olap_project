package com;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.*;

public class JaccardSimilarityTest {

    @Test
    public void testCalculateSimilarityCoefficient() {
        Set<Long> set1 = new HashSet<>();
        set1.add(1L);
        set1.add(2L);
        set1.add(3L);
        set1.add(4L);
        set1.add(5L);
        Set<Long> set2 = new HashSet<>();
        set2.add(2L);
        set2.add(3L);
        set2.add(4L);
        set2.add(6L);
        Assert.assertEquals(JaccardSimilarity.calculateSimilarityCoefficient(set1, set2), 0.5);

        set1.clear();
        set2.clear();

        set1.add(1L);
        set1.add(2L);
        set1.add(3L);
        set1.add(4L);
        set2.add(5L);
        set2.add(6L);
        set2.add(7L);
        set2.add(8L);

        Assert.assertEquals(JaccardSimilarity.calculateSimilarityCoefficient(set1, set2), 0.0);


        set1.clear();
        set2.clear();

        set1.add(1L);
        set1.add(2L);
        set1.add(3L);
        set1.add(4L);
        set2.add(1L);
        set2.add(2L);
        set2.add(3L);
        set2.add(4L);

        Assert.assertEquals(JaccardSimilarity.calculateSimilarityCoefficient(set1, set2), 1.0);


    }
}