package com;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;

public class FileSplitterTest {
    private static final String TEST_DIRECTORY = "test_split";

    private static final FileSplitter splitter = new FileSplitter(Constants.TEST_CONFIGURATION_FILENAME);
    private static final File testProperties = new File(Constants.TEST_CONFIGURATION_FILENAME);
    private static File file;
    private static File directory;
    private static File[] filesInDirectory;

    @BeforeClass
    public static void setUpToSplitFiles() {
        file = new File(FileSplitterTest.class.getClassLoader().getClass().getResource("/test.csv").getFile());
        directory = new File(TEST_DIRECTORY + "\\");
    }


    @Test
    public void testSplit() {
        splitter.split(file, TEST_DIRECTORY, 1_000_000);
        filesInDirectory = directory.listFiles();

        assert filesInDirectory != null;
        Assert.assertEquals(filesInDirectory.length, 10);
        Assert.assertEquals(testProperties.exists(), true);


    }

    @Test(dependsOnMethods = {"testSplit"})
    public void testGetFilesByTimestamps() {
        List<File> files;
        files = splitter.getFilesByTimestamps(1511739161, 1511741897);
        System.out.println(files.size());
        Assert.assertEquals(files.size(), 5);
    }


    protected void turnDownFiles() {
        for (File file1 : filesInDirectory) {
            file1.delete();
        }
        directory.delete();
        splitter.getProps().getConfigurationFile().delete();
    }

}