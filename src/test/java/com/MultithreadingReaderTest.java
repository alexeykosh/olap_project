package com;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import static org.testng.Assert.*;

public class MultithreadingReaderTest extends FileSplitterTest {
    private static MultithreadingReader reader;

    @BeforeClass
    public static void setUpReader() {
        reader = new MultithreadingReader(Constants.TEST_CONFIGURATION_FILENAME);
    }

    @Test(dependsOnMethods = {"testSplit"})
    public void testGetUniquePages() {
        Assert.assertEquals(reader.getUniquePages(), 370);
    }

    @Test(dependsOnMethods = {"testGetUniquePages"})
    public void testGetSimilarity() {
        Assert.assertEquals(reader.getSimilarity(15887, 17208, 1511739161, 1511742676), 0.1089994207935129);
    }

    @AfterClass
    public void turnDown() {
        super.turnDownFiles();
    }


}