package services;

import com.MultithreadingReader;
import com.Worker;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class Methods {
    private MultithreadingReader worker = Worker.getInstance();

    @GET
    @Path("pages")
    @Produces(MediaType.TEXT_PLAIN)
    public Response get() {
        return Response.status(200).entity("Unique pages: " + worker.getUniquePages()).build();
    }

    @GET
    @Path("similarity")
    @Produces(MediaType.TEXT_PLAIN)
    public Response similarity(@QueryParam("page1") long page1,
                               @QueryParam("page2") long page2,
                               @QueryParam("from") long timestamp1,
                               @QueryParam("to") long timestamp2) {
        double value = worker.getSimilarity(page1, page2, timestamp1, timestamp2);
        return Response.status(200).entity("Similarity: " + value).build();
    }

}
