package com;

import server.JettyServer;

import java.io.File;

public class Main {

    public static void main(String[] args) throws Exception {
        FileSplitter splitter = new FileSplitter(Constants.CONFIGURATION_FILENAME);

        if (args.length > 0) {
            File cvsFile = new File(args[0]);

            System.out.println("File reading started...");
            splitter.split(cvsFile, args[1]);
        }
//        File cvsFile = new File(path_to_file);
//        splitter.split(cvsFile, result_path);

        //вариант запроса к серверу.
//       /similarity?page1=15951&page2=13958&from=1511760909&to=1511770000

        JettyServer jettyServer = new JettyServer(8090);
        jettyServer.start();

    }

}
