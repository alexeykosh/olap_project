package com;

abstract class AbstractReader {
    protected static String DELIMITER = ",";
    protected static String EXT = ".csv";


    protected Long getParameter(Parameters parameter, String record) {
        switch (parameter) {
            case UID:
                return Long.valueOf(record.split(DELIMITER)[0]);
            case PAGE:
                return Long.valueOf(record.split(DELIMITER)[1]);
            case TIMESTAMP:
                return Long.valueOf(record.split(DELIMITER)[2]);
            default:
                throw new IllegalArgumentException("Wrong parameter");
        }
    }

}
