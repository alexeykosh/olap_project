package com;

public class FileDescriptor {
    private long fileID;
    private long firstTimestamp;
    private long lastTimestamp;

    public FileDescriptor(long fileID) {
        this.fileID = fileID;
    }

    public long getFirstTimestamp() {
        return firstTimestamp;
    }

    public long getFileID() {
        return fileID;
    }

    public void setFileID(long fileID) {
        this.fileID = fileID;
    }

    public void setFirstTimestamp(long firstTimestamp) {
        this.firstTimestamp = firstTimestamp;
    }

    public long getLastTimestamp() {
        return lastTimestamp;
    }

    public void setLastTimestamp(long lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }


}
