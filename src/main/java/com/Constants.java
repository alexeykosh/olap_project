package com;

public class Constants {
    public static final String CONFIGURATION_FILENAME = "properties.ini";
    public static final String TEST_CONFIGURATION_FILENAME = "test_properties.ini";
}
