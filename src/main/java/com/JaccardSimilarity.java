package com;

import java.util.Set;

public class JaccardSimilarity {

    public static double calculateSimilarityCoefficient(Set<Long> set1, Set<Long> set2) {
        if (set1.size() == 0 && set2.size() == 0) return 0;

        double coefficient;
        double a, b, c;

        a = set1.size();
        b = set2.size();

        set1.retainAll(set2);
        c = set1.size();

        coefficient = c / (a + b - c);

        return coefficient;
    }
}
