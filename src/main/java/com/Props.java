package com;

import java.io.*;
import java.util.List;

public class Props {
    private File configurationFile;

    public Props(String filename) {
        this.configurationFile = new File(filename);
    }

    public void create(String path, List<FileDescriptor> files) {

        try (Writer writer = new BufferedWriter(new FileWriter(configurationFile))) {
            writer.write("path=" + path + "\n");

            for (FileDescriptor file : files) {
                writer.write(file.getFileID() + "=" + file.getFirstTimestamp() + "/" + file.getLastTimestamp() + "\n");
                writer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String load(List<FileDescriptor> files) {
        String path = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(configurationFile))) {
            path = reader.readLine().split("=")[1];
            String line;
            while ((line = reader.readLine()) != null) {
                String[] strings = line.split("=");
                String timestamps[] = strings[1].split("/");

                FileDescriptor file = new FileDescriptor(Long.valueOf(strings[0]));
                file.setFirstTimestamp(Long.valueOf(timestamps[0]));
                file.setLastTimestamp(Long.valueOf(timestamps[1]));

                files.add(file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    public File getConfigurationFile() {
        return configurationFile;
    }
}
