package com;

public class Worker {

    private static MultithreadingReader ourInstance = new MultithreadingReader(Constants.CONFIGURATION_FILENAME);

    public static MultithreadingReader getInstance() {
        return ourInstance;
    }

    private Worker() {
    }
}
