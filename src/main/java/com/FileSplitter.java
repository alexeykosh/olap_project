package com;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

public class FileSplitter extends AbstractReader {
    private static int COUNT_OF_RECORDS_IN_FILE = 10_000_000;

    private List<FileDescriptor> fileDescriptors = new LinkedList<>();
    private Props props;

    protected int countOfFiles;
    protected String resultPath;

    public FileSplitter(String configFilename) {
        props = new Props(configFilename);

        if (props.getConfigurationFile().exists()) {
            resultPath = props.load(fileDescriptors);
            countOfFiles = fileDescriptors.size();
        }
    }

    public void split(File sourceFile, String path) {
        this.resultPath = path;

        int fileCounter = 1;
        int recordCounter = 0;

        FileWriter writer = null;
        FileDescriptor fileDescriptor = null;

        String currentLine = "";
        String nextLine;

        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new GZIPInputStream(
                                new FileInputStream(sourceFile))))) {

            File directory = new File(path);

            if (!directory.exists())
                directory.mkdir();


            while ((nextLine = reader.readLine()) != null) {
                currentLine = nextLine;

                if (recordCounter % COUNT_OF_RECORDS_IN_FILE == 0) {

                    if (writer != null) {

                        writerCloser(writer, fileDescriptor, currentLine);

                        currentLine = reader.readLine();
                    }


                    String filename = fileCounter + EXT;
                    writer = new FileWriter(resultPath + "\\" + filename);
                    fileDescriptor = new FileDescriptor(fileCounter);
                    fileDescriptor.setFirstTimestamp(getParameter(Parameters.TIMESTAMP, currentLine));

                    fileCounter++;
                    recordCounter = 1;
                }

                writer.write(currentLine + "\n");
                writer.flush();

                recordCounter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writerCloser(writer, fileDescriptor, currentLine);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        props.create(resultPath, fileDescriptors);
        this.countOfFiles = fileCounter;
    }

    public void split(File sourceFile, String path, int countOfRecord) {
        int defaultValue = COUNT_OF_RECORDS_IN_FILE;
        COUNT_OF_RECORDS_IN_FILE = countOfRecord;
        split(sourceFile, path);
        COUNT_OF_RECORDS_IN_FILE = defaultValue;
    }

    private void writerCloser(FileWriter writer, FileDescriptor fileDescriptor, String currentLine) throws IOException {
        fileDescriptor.setLastTimestamp(getParameter(Parameters.TIMESTAMP, currentLine));
        fileDescriptors.add(fileDescriptor);
        writer.close();
    }

    protected List<File> getFilesByTimestamps(long timestamp1, long timestamp2) {

        List<File> result = new LinkedList<>();

        for (FileDescriptor file : fileDescriptors) {
            if (timestamp1 <= file.getLastTimestamp() &&
                    timestamp2 >= file.getFirstTimestamp()) {
                result.add(new File(file.getFileID() + EXT));
            }
        }
        return result;
    }

    public Props getProps() {
        return props;
    }
}



