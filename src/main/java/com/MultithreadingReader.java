package com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArraySet;

public class MultithreadingReader extends FileSplitter {
    private Thread[] threads;
    private int countOfUniquePages = -1;

    public MultithreadingReader(String configFilename) {
        super(configFilename);
    }

    public int getUniquePages() {
        if (countOfUniquePages != -1) return countOfUniquePages;

        Set<Long> container = new ConcurrentSkipListSet<>();

        threads = new Thread[countOfFiles];
        for (int i = 0; i < threads.length; i++) {

            int finalI = i;

            threads[i] = new Thread(() -> {
                try (BufferedReader reader = new BufferedReader(new FileReader(resultPath + "\\" + (finalI + 1) + EXT))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        container.add(getParameter(Parameters.PAGE, line));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            threads[i].start();
        }
        waitOtherThreads();

        this.countOfUniquePages = container.size();

        return this.countOfUniquePages;
    }

    public double getSimilarity(long page1, long page2, long timestamp1, long timestamp2) {
        List<File> files = getFilesByTimestamps(timestamp1, timestamp2);
        Set<Long> set1 = new ConcurrentSkipListSet<>();
        Set<Long> set2 = new ConcurrentSkipListSet<>();

        threads = new Thread[files.size()];

        for (int i = 0; i < threads.length; i++) {

            int finalI = i;

            threads[i] = new Thread(() -> {
                try (BufferedReader reader = new BufferedReader(new FileReader(resultPath + "\\" + files.get(finalI)))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        long page = getParameter(Parameters.PAGE, line);
                        if (page == page1) set1.add(getParameter(Parameters.UID, line));
                        if (page == page2) set2.add(getParameter(Parameters.UID, line));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            threads[i].start();
        }

        waitOtherThreads();

        return JaccardSimilarity.calculateSimilarityCoefficient(set1, set2);
    }

    private void waitOtherThreads() {
        for (int i = 0; i < threads.length; i++) {
            if (threads[i].isAlive()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i--;
            }
        }
    }

}
